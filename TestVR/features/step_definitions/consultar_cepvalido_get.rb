Quando('faço um get na api via cep para consultar um cep valido') do
    $response = HTTParty.get("https://viacep.com.br/ws/01001000/json/")
  end
  
  Então('recebo as informações com o status code {int}') do |int|

    expect($response.code).to eq(200)
    puts "IBGE   : #{$response["ibge"]}"

  end