Quando('faço um get na api via cep com parametro invalido') do
    $response = HTTParty.get("https://viacep.com.br/ws/0000000/json/")
  end
  
  Então('recebo status code {int} sem informações') do |int|
    expect($response.code).to eql(400)
    puts "Status code : #{$response.code}"
  end
  