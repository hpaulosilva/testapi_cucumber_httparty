#language: pt

@consulta_invalida
Funcionalidade: Fazer uma consulta de cep invalido na api do via cep 
Como usuario desejo fazer uma consulta de cep 
invalido na api do via cep 

Cenario: Consultar cep invalido na api via cep
    Quando faço um get na api via cep com parametro invalido
    Então recebo status code 400 sem informações