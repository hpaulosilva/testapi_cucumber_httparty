#language: pt

@consulta
Funcionalidade: Fazer uma consulta na api do via cep 
Como usuario desejo fazer uma consulta de cep 
na api do via cep 

Cenario: Consultar cep na via api
    Quando faço um get na api via cep para consultar um cep valido
    Então recebo as informações com o status code 200